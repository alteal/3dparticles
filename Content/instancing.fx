﻿#if OPENGL
    #define SV_POSITION POSITION
    #define VS_SHADERMODEL vs_3_0
    #define PS_SHADERMODEL ps_3_0
#else
    #define VS_SHADERMODEL vs_4_0_level_9_1
    #define PS_SHADERMODEL ps_4_0_level_9_1
#endif

// Camera settings.
float4x4 World;
float4x4 View;
float4x4 Projection;

float3 LightDirection;
float Ambient;
float4 Color;

struct VertexShaderInput
{
    float4 Position : POSITION0;
    float3 Normal : NORMAL0;
};

struct VertexShaderOutput
{
    float4 Position : SV_POSITION;
    float4 Color : COLOR0;
	float LightingFactor : TEXCOORD0;
};

VertexShaderOutput VertexShaderFunction(VertexShaderInput input, float4x4 instanceTransform : BLENDWEIGHT)
{
    VertexShaderOutput output;

    // Apply the world and camera matrices to compute the output position.
    float4x4 instancePosition = mul(World, transpose(instanceTransform));
    float4 worldPosition = mul(input.Position, instancePosition);
    float4 viewPosition = mul(worldPosition, View);
    output.Position = mul(viewPosition, Projection);

	float3 Normal = normalize(mul(normalize(input.Normal), World));
	output.LightingFactor = 1;
	output.LightingFactor = saturate(dot(Normal, -LightDirection));
	output.Color = Color;

    return output;
}

float4 PixelShaderFunction(VertexShaderOutput input) : COLOR0
{
	float4 output = input.Color;
	output.rgb *= saturate(input.LightingFactor + Ambient);
    return output;
}

// Hardware instancing technique.
technique Instancing
{
    pass Pass1
    {
        VertexShader = compile VS_SHADERMODEL VertexShaderFunction();
        PixelShader = compile PS_SHADERMODEL PixelShaderFunction();
    }
}