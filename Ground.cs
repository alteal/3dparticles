﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;

namespace ParticleGround
{
    class ParticleGroundImpact
    {
        float intensity;
        public float lifeTime;
        float resistance;
        Vector2 location;

        public ParticleGroundImpact(Vector2 location, float intensity = 1.0f, float lifetime = 2000, float resistance = 0.1f)
        {
            this.location = location;
            this.intensity = intensity;
            this.resistance = resistance;
            this.lifeTime = lifetime;
        }
    }
    class ParticleGround
    {
        Model cube;
        Effect effect;

        Matrix view;
        Matrix projection;
        Vector3 viewVector;

        int width;
        int height;

        float gridSpace = 3.5f;

        VertexDeclaration instanceVertexDeclaration;
        DynamicVertexBuffer instanceVertexBuffer;

        Matrix[] positions;

        public ParticleGround(int width = 75, int height = 75)
        {
            this.width = width;
            this.height = height;
            positions = new Matrix[width * height];
        }

        public void Update(GameTime gameTime)
        {
            int radius = 55;
            int centerX = width / 2;
            int centerY = height / 2;
            for (int i = radius; i >= 0; i--)
            {
                List<ValueTuple<int, int>> circle = MidPointCircle(i, centerX, centerY);
                for (int j = 0; j < circle.Count(); j++)
                {
                    int x = circle[j].Item1;
                    int y = circle[j].Item2;

                    //Vector3 ViewTrans = Matrix.Invert(positions[x * width + y]).Translation;
                    float z = ImpactCalculation(gameTime, i);
                    Vector3 position = new Vector3(x * gridSpace - (width * gridSpace / 2), y * gridSpace - (height * gridSpace / 2), z);
                    positions[x * width + y] = Matrix.CreateTranslation(position);
                }
            }

            //for (int x = 0; x < width; x ++)
            //{
            //    for (int y = 0; y < height; y ++)
            //    {
            //        float frequency = 10;
            //        float time = (float)gameTime.TotalGameTime.TotalSeconds * frequency;
            //        float amplitude = 5;
            //        float period = 3;
            //        float z = (float)(amplitude * Math.Sin((y + x - time) / period));
            //        Vector3 position = new Vector3(x * gridSpace - (width * gridSpace / 2), y * gridSpace - (height * gridSpace / 2), z);
            //        positions[x * width + y] = Matrix.CreateTranslation(position);
            //    }
            //}

            instanceVertexBuffer.SetData(positions, 0, width * height, SetDataOptions.Discard);
        }

        public void Load(GraphicsDevice graphicsDevice, ContentManager content)
        {
            Vector3 lookat = new Vector3(0, -100, 200);
            view = Matrix.CreateLookAt(lookat, Vector3.Zero, Vector3.Up);
            projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, graphicsDevice.Viewport.AspectRatio, 1, 3000);

            viewVector = Vector3.Transform(Vector3.Zero - lookat, Matrix.CreateRotationY(0));
            viewVector.Normalize();

            instanceVertexDeclaration = new VertexDeclaration
            (
                new VertexElement(0, VertexElementFormat.Vector4, VertexElementUsage.BlendWeight, 0),
                new VertexElement(16, VertexElementFormat.Vector4, VertexElementUsage.BlendWeight, 1),
                new VertexElement(32, VertexElementFormat.Vector4, VertexElementUsage.BlendWeight, 2),
                new VertexElement(48, VertexElementFormat.Vector4, VertexElementUsage.BlendWeight, 3)
            );

            instanceVertexBuffer = new DynamicVertexBuffer(graphicsDevice, instanceVertexDeclaration, width * height, BufferUsage.WriteOnly);
            instanceVertexBuffer.SetData(positions, 0, width * height, SetDataOptions.Discard);

            effect = content.Load<Effect>("instancing");
            cube = content.Load<Model>("cube");

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    positions[x * width + y] = Matrix.CreateTranslation(new Vector3(x * gridSpace - (width * gridSpace / 2), y * gridSpace - (height * gridSpace / 2), 0));
                }
            }
        }

        void xLine(int x1, int x2, int y, List<ValueTuple<int,int>> circleData)
        {
            while (x1 <= x2)
            {
                if (x1 >= 0 && x1 < width && y >= 0 && y < height)
                    circleData.Add(new ValueTuple<int, int>(x1, y));
                x1 += 1;
            }
        }

        void yLine(int x, int y1, int y2, List<ValueTuple<int,int>> circleData)
        {
            while (y1 <= y2)
            {
                if (x >= 0 && x < width && y1 >= 0 && y1 < height)
                    circleData.Add(new ValueTuple<int, int>(x, y1));
                y1 += 1;
            }
        }

        public List<ValueTuple<int, int>> MidPointCircle(int radius, int xc, int yc, int thickness = 2)
        {
            List<ValueTuple<int, int>> positions = new List<ValueTuple<int, int>>();
            int inner = radius - thickness + 1;
            int xo = radius;
            int xi = inner;
            int y = 0;
            int erro = 1 - xo;
            int erri = 1 - xi;

            while (xo >= y)
            {
                xLine(xc + xi, xc + xo, yc + y, positions);
                yLine(xc + y, yc + xi, yc + xo, positions);
                xLine(xc - xo, xc - xi, yc + y, positions);
                yLine(xc - y, yc + xi, yc + xo, positions);
                xLine(xc - xo, xc - xi, yc - y, positions);
                yLine(xc - y, yc - xo, yc - xi, positions);
                xLine(xc + xi, xc + xo, yc - y, positions);
                yLine(xc + y, yc - xo, yc - xi, positions);

                y++;

                if (erro < 0)
                {
                    erro += 2 * y + 1;
                }
                else
                {
                    xo--;
                    erro += 2 * (y - xo + 1);
                }

                if (y > inner)
                {
                    xi = y;
                }
                else
                {
                    if (erri < 0)
                    {
                        erri += 2 * y + 1;
                    }
                    else
                    {
                        xi--;
                        erri += 2 * (y - xi + 1);
                    }
                }
            }

            return positions;
        }

        public float ImpactCalculation(GameTime gameTime, int d)
        {
            float frequency = 10;
            float time = (float)gameTime.TotalGameTime.TotalSeconds * frequency;
            float amplitude = 10;
            float period = 3;
            float z = (float)(amplitude * Math.Sin((d - time) / period));
            //float z = (float)(amplitude / (d - time * frequency) * Math.Sin((d - time * frequency) / period));
            return z;
        }

        public void Draw(GraphicsDevice graphicsDevice, GameTime gameTime)
        {
            foreach (ModelMesh mesh in cube.Meshes)
            {
                foreach (ModelMeshPart meshPart in mesh.MeshParts)
                {
                    graphicsDevice.SetVertexBuffers(
                           new VertexBufferBinding(meshPart.VertexBuffer, meshPart.VertexOffset, 0),
                           new VertexBufferBinding(instanceVertexBuffer, 0, 1)
                       );

                    graphicsDevice.Indices = meshPart.IndexBuffer;

                    Effect effect = this.effect;
                    effect.CurrentTechnique = effect.Techniques["Instancing"];

                    effect.Parameters["World"].SetValue(Matrix.CreateScale(1.5f) * Matrix.Identity);
                    effect.Parameters["View"].SetValue(view);
                    effect.Parameters["Projection"].SetValue(projection);


                    effect.Parameters["LightDirection"].SetValue(new Vector3(.3f, .5f, .8f));
                    effect.Parameters["Ambient"].SetValue(0.3f);
                    effect.Parameters["Color"].SetValue(new Vector4(0.3f, .6f, 0, 1f));
                    /*effect.Parameters["AmbientLight"].SetValue(Vector3.One);
                    effect.Parameters["DiffuseLight"].SetValue(Vector3.Zero);
                    effect.Parameters["LightDirection"].SetValue(Vector3.One);*/

                    foreach (var pass in effect.CurrentTechnique.Passes)
                    {
                        pass.Apply();

                        graphicsDevice.DrawInstancedPrimitives(PrimitiveType.TriangleList, 0, meshPart.StartIndex, meshPart.PrimitiveCount, positions.Length);
                    }
                }
            }
            //for (int i = 0; i < positions.Length; i++)
            //{
            //    foreach (ModelMesh mesh in cube.Meshes)
            //    {
            //        foreach (BasicEffect effect in mesh.Effects)
            //        {
            //            //float frequency = 10;
            //            //float time = (float)gameTime.TotalGameTime.TotalSeconds * frequency;
            //            //float amplitude = 10;
            //            //float period = 1;
            //            //float z = (float)(amplitude * Math.Sin((x + y - time) / period));
            //            //float z = (float)(amplitude / (x - time) * Math.Sin((x - time) / period));
            //            //effect.World = Matrix.CreateScale(.75f) * Matrix.Identity + Matrix.CreateTranslation(new Vector3(x * spacing - (width * spacing / 2), y * spacing - (height * spacing / 2), z));
            //            //float z = cubeHeights[x, y];
            //            effect.World = Matrix.CreateScale(.75f) * Matrix.Identity + Matrix.CreateTranslation(positions[i]);
            //        }
            //        mesh.Draw();
            //    }
            //}
        }
    }
}
